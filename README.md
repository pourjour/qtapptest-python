# Python QtAppTest

## Description

Python QtAppTest is a tool designed for end-to-end testing of python/PySide6 applications. This Python-based project comprises three key components:

* Server
* Client
* Examples

## Server

The server component is integrated into your C++/QtQuick application and operates as a WebSocket server. It receives remote commands to simulate user interactions with the application, such as keypresses, mouse clicks, and mouse movements.

To implement the server, include a few C++ files in your project and instantiate a `Server` within your `main()` function. This creates a WebSocket server within your application, ready to receive and process commands. It's advisable to enclose this with a conditional `#ifdef` switch to exclude it from your final release builds, using it exclusively for end-to-end testing.

## Client

The client is responsible for running testing scripts in Python. These scripts send commands to mimic user interactions with the application under test. The WebSocket server within the application receives and processes these commands. The server sends back the results of each command, which can be checked and verified.

To create testing scripts, import `qtapptest.py` and utilize the provided classes and functions. Example testing scripts are available in the examples folder.

## Examples

TODO

## Dependencies

The server component requires the Qt framework with additional `QtTest` and `QtWebSockets` modules. The client relies on Python and needs the following modules: `pytest` and `websocket-client`.

## How to Test Your Application

TODO

## License

MIT Licensed. Copyright 2023 Sofyane ELHADDAJY.
