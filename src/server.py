import json
from PySide6.QtCore import QObject, QTimer, Slot, Qt
from PySide6.QtGui import QGuiApplication, QWindow
from PySide6.QtWidgets import QApplication
from PySide6.QtNetwork import QHostAddress
from PySide6.QtWebSockets import QWebSocketServer, QWebSocket
from wave.commands import BaseCommands
class Server(QObject):
    def __init__(self, commands=None, port=2525, parent=None):
        super().__init__(parent)
        self.m_webSocketServer = QWebSocketServer("QtAppTester", QWebSocketServer.NonSecureMode, self)
        self.m_commands = None
        self.m_webSocket = None
        self.m_focusWindow = None
        self.m_reply = None

        assert QGuiApplication.instance() is not None

        QGuiApplication.instance().focusWindowChanged.connect(self.onFocusWindowChanged)
        self.onFocusWindowChanged(QGuiApplication.instance().focusWindow())

        self.setCommands(commands)

        if self.m_webSocketServer.listen(QHostAddress.Any, port):
            print(f"QtAppTest: listening on port {port}")
            self.m_webSocketServer.newConnection.connect(self.onSocketConnected)
        else:
            print(f"QtAppTest: NOT listening on port {port}")

    def setCommands(self, commands):
        self.m_commands = commands
        if self.m_commands is not None:
            self.m_commands.set_server(self)

    def commands(self):
        return self.m_commands

    @Slot()
    def onSocketConnected(self):
        if self.m_webSocket is not None:
            print("QtAppTest: client already connected")
            return

        self.m_webSocket = self.m_webSocketServer.nextPendingConnection()
        self.m_webSocket.setParent(self)
        self.m_webSocket.textMessageReceived.connect(self.processRequest)
        self.m_webSocket.disconnected.connect(self.onSocketDisconnected)

    @Slot()
    def onSocketDisconnected(self):
        self.m_webSocket.deleteLater()
        self.m_webSocket = None

    def focusWindow(self):
        if self.m_focusWindow is not None:
            return self.m_focusWindow
        else:
            self.m_focusWindow = QApplication.focusWindow()
            return self.m_focusWindow

    @Slot(QWindow)
    def onFocusWindowChanged(self, window):
        if window is not None:
            self.m_focusWindow = window

    @Slot(str)
    def processRequest(self, data):
        self.postSuccess()
        QTimer.singleShot(0, self.sendReply)

        obj = json.loads(data)
        if not obj:
            self.postError("QtAppTest: invalid JSON document")
            return

        if self.m_commands is None:
            # Assuming BaseCommands class has been implemented in Python
            self.setCommands(BaseCommands(self))

        self.activateFocusWindow()
        self.m_commands.processRequest(obj)

    def activateFocusWindow(self):
        if QGuiApplication.instance().focusWindow() != self.m_focusWindow and self.m_focusWindow is not None:
            self.m_focusWindow.show()
            self.m_focusWindow.raise_()
            self.m_focusWindow.setWindowState(Qt.WindowActive)
            self.m_focusWindow.requestActivate()

    def postValue(self, value):
        reply = {"success": True, "value": value}
        self.postReply(reply)

    def postSuccess(self):
        reply = {"success": True}
        self.postReply(reply)

    def postError(self, message):
        reply = {"success": False,"error": message}
        self.postReply(reply)

    def postReply(self, reply):
        self.m_reply = reply

    def sendReply(self):
        data = json.dumps(self.m_reply)
        self.m_webSocket.sendTextMessage(data)