from PySide6 import QtCore, QtWidgets, QtGui, QtQuick
from PySide6.QtTest import QTest
from enum import Enum

from PySide6.QtCore import Qt, QPointF, QCoreApplication, QPoint
from PySide6.QtTest import QTest
from PySide6.QtGui import QWindow
from PySide6.QtQuick import QQuickItem
from PySide6.QtWidgets import QWidget
import json


class KeyCommand(Enum):
    Click = 1
    Press = 2
    Release = 3


class AbstractCommands(QtCore.QObject):
    def __init__(self, server=None, parent=None):
        super(AbstractCommands, self).__init__(parent)
        self.server = server

    def processRequest(self, data):
        raise NotImplementedError

    def set_server(self, server):
        self.server = server


class BaseCommands(AbstractCommands):
    def __init__(self, server=None, parent=None):
        super(BaseCommands, self).__init__(server, parent)

    def processRequest(self, data):
        command = data.get("command", "")
        command = command.strip()
        print(f"command: '{command}'")  # Check the value of command

        # mapping each command to its corresponding method
        # command_dict = {
        #     "key_click": self.key_command,
        #     "key_press": self.key_command,
        #     "key_release": self.key_command,
        #     "write_text": self.write_text,
        #     # omitted other commands for brevity
        #     "quit": self.quit
        # }

        if command == "key_click":
            KeyCommand(KeyCommand.Click, data)
        elif command == "key_press":
            KeyCommand(KeyCommand.Press, data)
        elif command == "key_release":
            KeyCommand(KeyCommand.Release, data)
        elif command == "write_text":
            self.write_text(data)
        elif command == "mouse_click":
            self.mouse_command(QTest.MouseAction.MouseClick, data)
        elif command == "mouse_dbl_click":
            self.mouse_command(QTest.MouseAction.MouseDClick, data)
        elif command == "mouse_press":
            self.mouse_command(QTest.MouseAction.MousePress, data)
        elif command == "mouse_release":
            self.mouse_command(QTest.MouseAction.MousePress, data)
        elif command == "activate_window":
            self.activate_window(data)
        elif command == "get_active_window":
            self.get_active_window(data)
        elif command == "get_property":
            self.get_property(data)
        elif command == "quit":
            quit()
        else:
            self.server.postError(f"unknown command {command}")

    def quit(self):
        QtCore.QCoreApplication.sendPostedEvents()
        QtCore.QCoreApplication.processEvents()
        QtCore.QCoreApplication.quit()


    def get_focused_window(self):
        window = QtWidgets.QApplication.focusWindow()
        if window is None:
            self.server.postError("no focus window")
        return window

    def write_text(self, data):
        window = self.get_focused_window()
        print(window)
        if window:
            text = data.get("text", "")
            for letter in text:
                QTest.keyClick(window, letter)

    def key_command(self, command: KeyCommand, data):
        window = self.acquire_focus_window()
        window.setFocus()
        if not window:
            return

        key_str = data.get("key", "")
        if not key_str:
            self.server.post_error("undefined 'key' parameter")
            return

        key_sequence = QtGui.QKeySequence(key_str)
        if key_sequence.count() == 0:
            self.server.post_error("invalid 'key' parameter")
            return

        key = key_sequence[0].key()
        modifiers = key_sequence[0].keyboardModifiers()

        if command == KeyCommand.Click:
            QTest.keyClick(window, key, modifiers)
        elif command == KeyCommand.Press:
            QTest.keyPress(window, key, modifiers)
        elif command == KeyCommand.Release:
            QTest.keyRelease(window, key, modifiers)

    def mouse_command(self, action, data_json):
        # data = json.loads(data_json)  # Assuming 'data_json' is a JSON string
        data = data_json
        window = self.acquire_focus_window()
        window.setFocus()
        if window is None:
            return

        # TODO: implement key modifiers
        button_str = data.get("button", "").lower()
        button = None

        if not button_str:
            button = Qt.NoButton if action == QTest.MouseAction.MouseMove else Qt.LeftButton
        elif button_str == "left":
            button = Qt.LeftButton
        elif button_str == "right":
            button = Qt.RightButton
        elif button_str == "middle":
            button = Qt.MiddleButton
        else:
            self.server.postError("invalid 'button' parameter")
            return

        object_name = data.get("object_name", "")
        aquired_object = None

        if object_name:
            # TODO: we probably do not need window for finding quick item
            aquired_object = window.findChild(QWidget, object_name)
            aquired_object.setFocus()
            if aquired_object is None:
                self.server.postError(f"QWidget with aquired_object name '{object_name}' not found")
                return

        x = y = 0.0
        if aquired_object:
            print("aquired_object:", aquired_object)
            print(aquired_object.parent())
            # top_left = aquired_object.mapToScene(QPointF(0.0, 0.0))
            # top_left = aquired_object.mapTo(aquired_object.parent(), QPointF(0.0,0.0))
            top_left = aquired_object.mapToGlobal(aquired_object.rect().topLeft())
            print(top_left)
            x = top_left.x() + data.get("x", aquired_object.width() / 2.0)
            y = top_left.y() + data.get("y", aquired_object.height() / 2.0)
            print(x, y)
        else:
            # TODO: check missing param
            x = data.get("x", 0.0)
            y = data.get("y", 0.0)

        # QTest.mouseEvent(action, window, button, Qt.NoModifier, QPoint(x, y))
        # QTest.mouseClick(aquired_object, button, Qt.NoModifier, QPoint(x, y))
        QTest.mouseClick(aquired_object, button)
    def activate_window(self, data_json):
        data = data_json
        # data = json.loads(data_json)  # Assuming 'data_json' is a JSON string
        window = self.server.focusWindow()

        if window:
            window.show()
            window.raise_()
            window.setWindowState(Qt.WindowActive)
            window.requestActivate()
            return window
        else:
            self.server.postError("no focus window")
            return None

    def get_active_window(self, data_json):
        data = json.loads(data_json)  # Assuming 'data_json' is a JSON string
        addr = id(QtWidgets.QApplication.activeWindow())
        self.server.postValue(addr)

    def get_property(self, data_json):
        # data = json.loads(data_json)  # Assuming 'data_json' is a JSON string
        print(data_json)
        data = data_json
        aquired_object = self.acquire_quick_item(data)

        if aquired_object is None:
            return

        property_name = data["property_name"]
        self.server.postValue(aquired_object.property(property_name))

    def acquire_focus_window(self):
        # window = QtWidgets.QApplication.focusWindow()
        window = QtWidgets.QApplication.activeWindow()
        if window is None:
            self.server.postError("no focus window")
        return window

    def acquire_quick_item(self, data):
        object_name = data.get("object_name", "")
        if object_name:
            window = self.acquire_focus_window()
            # child_items = window.findChildren(QWidget)

            # Loop through the child items and print their details
            for item in window.findChildren(QWidget):
                print(item)
            print(window)
            if window is None:
                return None
            acquired_object = window.findChild(QWidget, object_name)
            if acquired_object is None:
                self.server.postError(f"QWidget with acquired_object name '{object_name}' not found")
            else:
                acquired_object.setFocus()
            return acquired_object
        self.server.postError("failed to acquire QWidget")
        return None
