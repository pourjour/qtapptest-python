import pytest
import os
import time
from qtapptest import QtApplication
from PySide6.QtTest import QTest

os.environ["APP_PATH"] = "C:\\Users\\math0\\OneDrive\\Documents\\GitHub\\ronanlabs\\target\\Wave\\Wave.exe"


@pytest.fixture
def start_app():
    app = QtApplication()
    app.delay = 2  # to slow down the test for easier visualization
    app.verbose = True  # to print websocket requests and replies
    app.activate()
    yield app
    app.finalize()


def get_square_pos(app):
    return app.get_property("square", "x"), app.get_property("square", "y")

def input_box_text(app):
    return app.get_property(object_name="username", property_name="text")


def test_clear_input_box(start_app):
    app = start_app
    # QTest.qWait(20000)
    app.write_text("username")
    assert input_box_text(app), "username"
    app.mouse_click(object_name="password")
    app.write_text("password")
    app.mouse_click(object_name="checkBox_login_terms")
    app.mouse_click(object_name="signIn")
    # time.sleep(30)
    # QTest.qWait(30000)
    # app.mouse_click(object_name="clearButton")
    # assert (input_box_text(app), "")

    # app.quit()

# def test_draq_square(start_app):
#     app = start_app
#
#     app.mouse_move(pos=(500,600))
#     time.sleep(2)
#     app.mouse_move(pos=(100,300))
#     time.sleep(2)
#     app.mouse_move(pos=(300,200))
#     time.sleep(2)
#     app.mouse_move(pos=(200,100))
#     time.sleep(2)
#     app.mouse_move(pos=(700,500))
#     time.sleep(2)
#
#     app.mouse_click(object_name="signIn")
#     time.sleep(2)
#
#     # Get the string inside the signIn button
#     signIn_text = app.get_property("signIn", "text")  # replace "text" with the property name that contains the string inside the button
#
#     # Assert that the string inside the signIn button is "test"
#     assert signIn_text == "Sign In", f'Expected the text to be "test", but got "{signIn_text}"'
#
#
