import json
import os
import subprocess
import time
import websocket


class ApplicationStartError(RuntimeError):
    """
    Raised when the application cannot be started.
    """


class SocketConnectionError(RuntimeError):
    """
    Raised when websocket connection with the application cannot be established.
    """


class CommandError(RuntimeError):
    """
    Raised when command cannot be executed by the application.
    """


class QtApplication(object):
    """
    Generic Qt application tester class using web sockets.
    Can be used for remote testing of any Qt-based application.
    """

    def __init__(self, executable=None, params=None, port=2525, offscreen=False, muted=True):
        """
        executable: The path to the tested application. It makes sense only when a new instance
                    of tested application is started. I.e. it is ignored when attaching to
                    already running instance of tested application. If this is empty,
                    then environment variable APP_PATH is used as a fallback value.
        params:     Custom parameters passed to the command line when a new instance of
                    the tested application gets started. It is ignored when attaching to
                    already running instance of tested application.
        muted:      True to suppress messages printed from within the tested application.
                    Note that when attaching to already running application
                    the messages are always muted regardless of this parameter.
        offscreen:  Application runs in off-screen mode.
        delay:      Wait time in seconds after any user interaction related
                    command has been sent to the application.
                    Non-zero wait time can be used for easily observable replay of tests.
                    Use the default value of zero to run the test at the highest speed.
        verbose:    Use True to output information from the test, e.g. the values of each
                    websocket request and reply. This can be used to debug the tests.
        """

        # these variables can be set only before starting the application
        self.executable = executable
        self.params = params
        self.port = port
        self.offscreen = offscreen  # Note: this parameter does not work yet
        self.muted = muted

        # these variables can be changed at any time during the test
        self.delay = 0.0
        self.verbose = False

        # initialized when the tested application is started
        self._process = None

        # initialized when the tested application is connected to
        self._socket = None

    def start(self):
        """
        Starts a new instance of tested application and makes a connection to it.
        """
        # TODO: custom exception if executable not defined
        executable = self.executable if self.executable else os.environ["APP_PATH"]
        cmd = [executable]

        # if self.offscreen:
        #    cmd += ["-platform", "offscreen"]

        if self.params:
            cmd += self.params

        if self.verbose:
            print("running: ", cmd)

        if self.muted:
            self._process = subprocess.Popen(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        else:
            self._process = subprocess.Popen(cmd, shell=True, creationflags=subprocess.CREATE_NEW_CONSOLE)

        self._process.poll()
        if self._process.returncode is None:
            self._connect(timeout=30.0)
        else:
            raise ApplicationStartError()

    def attach(self):
        """
        Makes a connection to already running instance of tested application.
        """
        self._connect(timeout=0.0)
        self.activate_window()

    def activate(self):
        """
        If the tested application is already running, then it attaches to it.
        If the tested application is not running, then starts a new instance.
        """
        try:
            self.attach()
        except SocketConnectionError:
            self.start()

    def finalize(self):
        """
        Finalize the run of the tested application.
        Releases resources even if the tested application did not quit properly (e.g. crashed).
        This should be called at the end of test fixtures.
        """
        if self._socket:
            self._socket.close()

        if self._process:
            self._process.kill()

    def send_command(self, request_data, check_reply=True):
        # TODO: add timeout which will raise error
        request = json.dumps(request_data)
        attempt = 0

        while True:
            self._socket.send(request)

            self._print_verbose("request: " + str(request_data))

            if not check_reply:
                return None

            reply = self._socket.recv()
            reply_data = json.loads(reply)
            self._print_verbose("reply: " + str(reply_data))
            success = reply_data["success"]

            # TODO: this is not proper error handling
            if not success:
                if reply_data["error"] == "no widget is focused":
                    attempt += 1
                    if attempt < 10:
                        time.sleep(0.1)
                        continue

                raise CommandError(reply_data["error"])

            try:
                return reply_data["value"]
            except KeyError:
                return None

    def quit(self, timeout=5.0):
        """
        Quit the tested application. This should be the last command in each test.
        """
        # closing the application closes the connection
        # so we do not wait for the reply
        if self._socket:
            self.send_command({"command": "quit"}, False)

        if self._process:
            try:
                self._process.wait(timeout)
            except subprocess.TimeoutExpired as e:
                self._process.kill()
                raise e

            if self._process.returncode != 0:
                raise RuntimeError(
                    "process did not exit correctly (return code = {0})".format(self._process.returncode))

            self._process = None

        if self._socket:
            self._socket.close()
            self._socket = None

    def key_click(self, key):
        """
        Sends key click (press followed by release) to the focused object.
        Usage: key_click("Ctrl+Home") or key_click("Right") or key_click("X")
        """
        self.send_command({"command": "key_click", "key": key})
        time.sleep(self.delay)

    def key_press(self, key):
        """
        Sends key press (without release) to the focused object.
        Usage: key_press("Ctrl+Home") or key_press("Right") or key_press("X")
        """
        self.send_command({"command": "key_press", "key": key})
        time.sleep(self.delay)

    def key_release(self, key):
        """
        Sends key release to the focused object.
        Usage: key_release("Ctrl+Home") or key_release("Right") or key_release("X")
        """
        self.send_command({"command": "key_release", "key": key})
        time.sleep(self.delay)

    def write_text(self, text):
        """
        Sends a text to the focused object.
        """
        self.send_command({"command": "write_text", "text": text})
        time.sleep(self.delay)

    def mouse_click(self, button="left", object_name=None, pos=None):
        """
        Mouse button click (press and release).
        Use object_name parameter to specify the target object.
        Use pos as (x, y) tuple to specify the click coordinates.
        At least one of object_name and pos must be specified.
        If both are specified, then pos represents local coordinates.
        """
        self._send_mouse_button_command("mouse_click", button=button, object_name=object_name, pos=pos)

    def mouse_dbl_click(self, button="left", object_name=None, pos=None):
        """
        Mouse button double click.
        Use object_name parameter to specify the target object.
        Use pos as (x, y) tuple to specify the click coordinates.
        At least one of object_name and pos must be specified.
        If both are specified, then pos represents local coordinates.
        """
        self._send_mouse_button_command("mouse_dbl_click", button=button, object_name=object_name, pos=pos)

    def mouse_press(self, button="left", object_name=None, pos=None):
        """
        Mouse button press (without release).
        Use object_name parameter to specify the target object.
        Use pos as (x, y) tuple to specify the click coordinates.
        At least one of object_name and pos must be specified.
        If both are specified, then pos represents local coordinates.
        """
        self._send_mouse_button_command("mouse_press", button=button, object_name=object_name, pos=pos)

    def mouse_release(self, button="left", object_name=None, pos=None):
        """
        Mouse button release.
        Use object_name parameter to specify the target object.
        Use pos as (x, y) tuple to specify the click coordinates.
        At least one of object_name and pos must be specified.
        If both are specified, then pos represents local coordinates.
        """
        self._send_mouse_button_command("mouse_release", button=button, object_name=object_name, pos=pos)

    def mouse_move(self, object_name=None, pos=None):
        """
        Move mouse cursor to target position.
        Use object_name parameter to specify the target object.
        Use pos as (x, y) tuple to specify the click coordinates.
        At least one of object_name and pos must be specified.
        If both are specified, then pos represents local coordinates.
        """
        self._send_mouse_button_command("mouse_move", button=None, object_name=object_name, pos=pos)

    def mouse_drag(self, x, y, dx, dy, button="left"):
        """
        Use object_name parameter to specify the target object.
        """
        # TODO: improve API
        delay = self.delay
        self.delay = 0
        self.mouse_press(button=button, pos=(x, y))
        dummy_distance = 20  # sufficient start-drag distance
        x += dummy_distance  # the direction does not matter
        self.mouse_move(pos=(x, y))
        x += dx
        y += dy
        self.mouse_move(pos=(x, y))
        self.mouse_release(button=button, pos=(x, y))
        self.delay = delay
        time.sleep(self.delay)

    def activate_window(self):
        self.send_command({"command": "activate_window"})
        # TODO: check if the window is activated

    def get_property(self, object_name, property_name):
        return self.send_command({"command": "get_property",
                                  "object_name": object_name,
                                  "property_name": property_name})

    def _connect(self, timeout):
        """
        Create a web socket connection to a running instance of the application.
        """
        assert self._socket is None

        url = "ws://localhost:" + str(self.port)
        self._print_verbose("connecting to {0}".format(url))
        end_time = time.time() + timeout

        while True:
            try:
                self._socket = websocket.create_connection(url)
                assert self._socket is not None
                self._print_verbose("connection successful")
                return

            except Exception:  # TODO: specific exception?
                assert self._socket is None

            if time.time() > end_time:
                self._print_verbose("connection failed")
                if self._process:
                    self._process.kill()
                    self._process = None
                raise SocketConnectionError()

            time.sleep(0.1)

    def _send_mouse_button_command(self, command, button, object_name, pos):
        args = {"command": command}
        if button:
            args["button"] = button
        if object_name:
            args["object_name"] = object_name
        if pos:
            args["x"] = pos[0]
            args["y"] = pos[1]
        self.send_command(args)
        time.sleep(self.delay)

    def _print_verbose(self, message):
        if self.verbose:
            print(message)
